package com.revature.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "posts", indexes = {
        @Index(name = "idx_post_user_id", columnList = "user_id")})
@Data
@NoArgsConstructor
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="post_id")
    private Integer id;
    @Column(name ="body", length = 10000)
    private String body;

    @Column(name = "date")
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd @HH:mm:ss", timezone="EST")
    private Timestamp date;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User user;


    public Post(String body, User user) {
        this.body = body;
        this.user = user;
    }
    public Post(Integer id){
        this.id = id;
    }

}
