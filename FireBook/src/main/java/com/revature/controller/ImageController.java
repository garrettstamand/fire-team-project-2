package com.revature.controller;

import com.revature.model.Image;
import com.revature.model.JsonResponse;
import com.revature.model.Post;
import com.revature.repository.ImageDao;
import com.revature.service.ImageService;
import com.revature.service.S3Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.net.URL;
import java.util.List;

@RestController("imageController")
@CrossOrigin(value = "http://localhost:4200", allowCredentials = "true") //this is to fix CORS AND sessions
@RequestMapping(value = "api/image")
public class ImageController {
    ImageService imageService;
    S3Services s3Services = new S3Services();

    @Autowired
    public ImageController(ImageService imageService){this.imageService = imageService;}

    @GetMapping("")
    public JsonResponse getAllImages(){
        return new JsonResponse(true, "lists of all Images", this.imageService.getAllImages());
    }

    @PostMapping("")
    public JsonResponse createImage(@RequestBody Image image){
        System.out.println(image);
        return new JsonResponse(true, "Image successfully added", this.imageService.createImage(image));}

    @GetMapping("get-image-by-postId/{postId}")
    public JsonResponse getImageByPostId(@PathVariable("postId") Integer postId){
        JsonResponse jsonResponse;
        List<Image> images = this.imageService.getImageByPostId(postId);
        if (images != null){
            jsonResponse = new JsonResponse(true, "List of images", images);
        } else {
            jsonResponse = new JsonResponse(false, "Image not found", null);
        }
        return jsonResponse;
    }


}
