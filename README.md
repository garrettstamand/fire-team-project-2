# FireBook

## Project Description
FireBook is a social media application that allows users to customize their profiles, post new contents, and comment on other users' posts. Additionally, users can view other peoples' profiles and view specific posts they have made.

## Technologies Used
- Maven
- Java
- Spring MVC
- Spring Data
- Spring Boot
- PostgreSQL
- Typescript
- Angular
- HTML
- CSS

## Features
- Users can upload multiple pictures in a single post
- Fully functional like button which shows who else liked a post on hover
- Comments can be added to posts
- Persists changes to a database
- Uses bcrypt to encrypt user passwords

## Getting Started
1. Using GitBash, use the 'cd' command to change directories until you are where you want the project to download to.
2. Type in 'git clone https://gitlab.com/garrettstamand/fire-team-project-2.git'
3. Download and install JDK 8 (https://www.oracle.com/java/technologies/downloads/)
4. Download and install Node.js (https://nodejs.org/en/download/)

## Usage
1. Open a Java IDE of your choice (I prefer Intellij).
2. Ensure you set the environment variables for "AWS_URL", "AWS_USERNAME", and "AWS_PASSWORD". If you'd prefer to use an H2 database, set that option in application.properties.
3. Run the main method.
4. Using a command line, navigate to where you downloaded the repository then go into the Angular/frontend folder.
5. Type npm install to ensure you have the required packages.
6. Type 'npm serve -o'. A browser window will open and take you to the website.
