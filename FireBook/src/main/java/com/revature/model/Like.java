package com.revature.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "likes")
public class Like {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="like_id")
    private Integer id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Post post;

    public Like(Integer userId, Integer postId){
        this.user = new User(userId);
        this.post = new Post(postId);
    }

}
