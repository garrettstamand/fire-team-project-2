import { Component, OnInit } from '@angular/core';
import { ResetService } from 'src/app/services/reset.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  _email: string = "";
  _enterResetDisplay: string = "none";
  _enterNewDisplay: string = "none";
  _token: string = "";
  _objectEmail: string = "";
  _newPassword: string = "";
  _message: string = "";

  constructor(private apiServ: ResetService, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    //Check user session
    this.userService.checkSession().subscribe(data => {
      if (data.success == true)
        this.router.navigate(['/profile']);
    });
  }

  reset() {
    this._enterNewDisplay = "none";
    this._enterResetDisplay = "none";
  }

  sendEmail() {
    this.apiServ.checkEmail(this._email).subscribe(data => {
      console.log(data);
      if (data.success == true) {
        this._message = "Email Sent"
        this._enterResetDisplay = "";
        this.apiServ.createToken(data.object).subscribe(tokenData => {
          console.log(tokenData);
          this._objectEmail = tokenData.object.userEmail;
          console.log(this._objectEmail);
        })
      } else {
        this._message = "Email Not Found"
      }
    })
  }

  compareToken() {
    this.apiServ.compareToken(this._token, this._objectEmail).subscribe(data => {
      console.log(data);
      if (data.success == true) {
        this._enterNewDisplay = "";
        this._message = "Tokens Match";
      } else {
        this._message = "Tokens Do Not Match"
      }
    })
  }

  resetPass() {
    this.apiServ.updatePassword(this._newPassword, this._objectEmail).subscribe(data => {
      console.log(data);
      if (data.success == true) {
        this.reset();
        this._message = "Password Reset";
      } else {
        this._message = "An Error Has Occured"
      }
    })
  }

}
