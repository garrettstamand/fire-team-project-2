package com.revature.service;

import com.revature.mail.Mail;
import com.revature.model.User;
import com.revature.repository.UserDao;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserService {
    private UserDao userDao;
    private Mail mail = new Mail();

    @Autowired
    public UserService(UserDao userDao){
        this.userDao = userDao;
    }

    public List<User> getAllUsers(){
        return this.userDao.findAll();
    }

    public User createUser(User user){
        User tempUser = this.userDao.findUserByUsername(user.getUsername()).orElse(null);
        User tempUserEmail = this.userDao.findUserByEmail(user.getEmail()).orElse(null);
        if(tempUser == null && tempUserEmail == null){
            String hash = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(6));
            user.setPassword(hash);
            mail.welcomeMail(user);
            return this.userDao.save(user);
        }
        return null;
    }

    public User getUserByUsername(String username) {
        return this.userDao.findUserByUsername(username).orElse(null);
    }

    public User getUserByEmail(String email) { return this.userDao.findUserByEmail(email).orElse(null); }

    public User getUserById(Integer id) { return this.userDao.findUserById(id).orElse(null); }

    public void updatePassword(String password, String email) {
        User tempUser = this.userDao.findUserByEmail(email).orElse(null);
        String hash = BCrypt.hashpw(password, BCrypt.gensalt(6));
        tempUser.setPassword(hash);
        this.userDao.save(tempUser);
    }

    public User login(User user){
        User tempUser = userDao.findUserByUsername(user.getUsername()).orElse(null);
        if(tempUser!=null){
            if(BCrypt.checkpw(user.getPassword(), tempUser.getPassword())){
                return tempUser;
            }
        }
        return null;
    }
    public User updateUser(User user){
        User tempUser = this.userDao.findById(user.getId()).orElse(null);

        if(tempUser!= null){
            if(!user.getUsername().equals("")){
                tempUser.setUsername(user.getUsername());
            }
            if(!user.getPassword().equals("")){
                String hash = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(6));
                tempUser.setPassword(hash);
            }
            if(!user.getFirstName().equals("")){
                tempUser.setFirstName(user.getFirstName());
            }
            if(!user.getLastName().equals("")){
                tempUser.setLastName(user.getLastName());
            }
            if(!user.getEmail().equals("")){
                tempUser.setEmail(user.getEmail());
            }
            if(!user.getUrl().equals("")){
                tempUser.setUrl(user.getUrl());
            }
            return this.userDao.save(tempUser);
        }
        return null;
    }
}
