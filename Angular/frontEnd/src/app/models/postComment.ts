
export class PostComment{
    id: number = 0;
    userId: number = 0;
    postId: number = 0;
    firstName: string = '';
    lastName: string = '';
    body: string = '';
    date: Date = new Date();
    url: string = '';

    constructor(id: number, userId: number, postId: number, firstName: string, lastName: string, body: string, date: Date, url: string){
        this.id = id;
        this.userId = userId;
        this.postId = postId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.body = body;
        this.date = date;
        this.url = url;
    }
}
