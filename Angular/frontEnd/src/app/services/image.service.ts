import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlConstant } from '../globals/url';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private httpClient: HttpClient) { }

  createImage(newId: number, imageUrl: string) : Observable<any>{
    return this.httpClient.post(UrlConstant.apiURL + "/api/image",  {
      url: imageUrl,
      post:{
        id: newId
      }
    }, {withCredentials: true});
  };

  getImageById(postId: number) : Observable<any>{
    return this.httpClient.get(UrlConstant.apiURL + "/api/image/get-image-by-postId/" + postId, {withCredentials: true});
  }

}
