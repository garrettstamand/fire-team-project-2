import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-friendslist',
  templateUrl: './friendslist.component.html',
  styleUrls: ['./friendslist.component.css']
})
export class FriendslistComponent implements OnInit {

  userList: any[] = [];
  searchParameter: string = '';

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUserList();
  }

  getUserList(){
    this.userList = [];
    this.userService.getAllUsers().subscribe((jsonReponse)=>{
      jsonReponse.object.forEach(user => {
        if(user.url == null) user.url = 'assets/images/EmptyProfile.png'
        let param = this.searchParameter.toLowerCase();
        if(user.firstName.toLowerCase().includes(param) || user.lastName.toLowerCase().includes(param) || (user.firstName.toLowerCase() + " " + user.lastName.toLowerCase()).includes(param)) this.userList.push(user);
      });
    })
  }


}
