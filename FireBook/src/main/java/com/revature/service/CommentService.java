package com.revature.service;

import com.revature.model.Comment;
import com.revature.repository.CommentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("commentService")
public class CommentService {
    private CommentDao commentDao;

    @Autowired
    public CommentService(CommentDao commentDao){this.commentDao = commentDao;}

    public Comment createComment(Comment comment){return this.commentDao.save(comment);}

    public List<Comment> getAllComments(){
        return this.commentDao.findAll();
    }

    public List<Comment> findCommentsByPostId(Integer postId){
        return this.commentDao.findCommentByPostId(postId);
    }

}
