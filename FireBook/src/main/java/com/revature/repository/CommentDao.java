package com.revature.repository;

import com.revature.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository("commentDao")
@Transactional
public interface CommentDao extends JpaRepository<Comment, Integer> {

    @Query("from Comment where post.id = :postId")
    List<Comment> findCommentByPostId(@Param("postId") Integer postId);
}
