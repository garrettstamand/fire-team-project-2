import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { PostComment } from 'src/app/models/postComment';
import { CommentService } from 'src/app/services/comment.service';

@Component({
  selector: 'app-newcomment',
  templateUrl: './newcomment.component.html',
  styleUrls: ['./newcomment.component.css']
})
export class NewcommentComponent implements OnInit {

  newCommentBody: string = "";
  @Input()
  postId: number = 0;
  @Output()
  updateParent: EventEmitter<any> = new EventEmitter();

  constructor(private commentService: CommentService) { }

  ngOnInit(): void {
  }


  submitComment(){
    this.commentService.createComment(new PostComment(0, 0, this.postId, "", "", this.newCommentBody, new Date(), "")).subscribe();
    this.updateParent.emit();
  }

}
