import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadpicsComponent } from './uploadpics.component';

describe('UploadpicsComponent', () => {
  let component: UploadpicsComponent;
  let fixture: ComponentFixture<UploadpicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadpicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadpicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
