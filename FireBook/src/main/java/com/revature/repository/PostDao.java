package com.revature.repository;

import com.revature.model.JsonResponse;
import com.revature.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository("postDao")
@Transactional
public interface PostDao extends JpaRepository<Post, Integer> {

    @Query("select post.user, post.id, post.body, post.date from Post as post INNER JOIN post.user")
    Optional<List> getPostAndUser();

    @Query("from Post as post where post.user.id = :userId")
    Optional<Page> getPostByUserId(@Param("userId") Integer userId, Pageable pageable);

    @Query("from Post as post")
    Optional<Page> getPosts(@Param("userId") Integer userId, Pageable pageable);
}
