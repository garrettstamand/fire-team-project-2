package com.revature.controller;

import com.revature.mail.Mail;
import com.revature.model.JsonResponse;
import com.revature.model.PasswordReset;
import com.revature.repository.UserDao;
import com.revature.service.PassService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController("passController")
@CrossOrigin(value = "http://localhost:4200", allowCredentials = "true") //this is to fix CORS AND sessions
@RequestMapping(value = "api")
public class PassController {
    private PassService passService;

    @Autowired
    public PassController(PassService passService) {
        this.passService = passService;
    }

    @GetMapping("createToken/{email}")
    public JsonResponse createToken(@PathVariable String email) {
        if (passService.getToken(email) != null) { //Checks to see if there is already a token
            passService.deleteByEmail(email);
        }
        String token = BCrypt.gensalt(6);
        PasswordReset passwordReset = new PasswordReset(email, token);
        Mail mail = new Mail();
        mail.sendMail(email, token);
        passService.createToken(passwordReset);
        return new JsonResponse(true, "email sent!", passwordReset);
    }

    @PostMapping("compareToken")
    public JsonResponse compareToken(@RequestBody PasswordReset pReset) {
        String email = pReset.getUserEmail();
        String token = pReset.getToken();
        System.out.println(token + email);
        PasswordReset passwordReset = this.passService.getToken(email);
        if (passwordReset.getToken().equals(token)) {
            return new JsonResponse(true, "tokens match", email);
        } else {
            return new JsonResponse(false, "token do NOT match", null);
        }
    }
}