import { Component, Input, OnInit } from '@angular/core';
import { PostComment } from 'src/app/models/postComment';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input()
  comment: PostComment = new PostComment(0, 0, 0, "", "", "", new Date(), "");

  constructor() { }

  ngOnInit(): void {
  }

}
