package com.revature.service;

import com.revature.model.JsonResponse;
import com.revature.model.Post;
import com.revature.repository.PostDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("postService")
public class PostService {
    private PostDao postDao;

    @Autowired
    public PostService(PostDao postDao){
        this.postDao = postDao;
    }

    public List<Post> getAllPosts(){return this.postDao.findAll();}

    public Post createPost(Post post){
        return this.postDao.save(post);
    }

    public List getPostAndUser(){
        return this.postDao.getPostAndUser().orElse(null);
    }

    public List findPostByUserId(Integer userId, Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by("date").descending());
        Page<Post> pagedPost;
        if(userId == 0) pagedPost = postDao.getPosts(userId, paging).orElse(null);
        else pagedPost = postDao.getPostByUserId(userId, paging).orElse(null);

        if (pagedPost.hasContent()) {
            return pagedPost.getContent();
        } else {
            return new ArrayList<Post>();
        }
    }



}
