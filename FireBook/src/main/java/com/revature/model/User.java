package com.revature.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id;
    @Column(name ="username", unique = true, nullable = false)
    private String username;
    @Column(name ="password", nullable = false)
    private String password;
    @Column(name ="firstname", nullable = false)
    private String firstName;
    @Column(name ="lastname", nullable = false)
    private String lastName;
    @Column(name ="email", unique = true, nullable = false)
    private String email;
    @Column(name="url")
    private String url;

    public User(String username, String password, String firstName, String lastName, String email) {

        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;

    }
    public User(Integer id){
        this.id = id;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String email, String password, int filler) {
        this.email = email;
        this. password = password;
        filler = 0;
    }
}
