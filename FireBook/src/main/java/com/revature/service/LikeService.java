package com.revature.service;

import com.revature.model.Comment;
import com.revature.model.Like;
import com.revature.repository.CommentDao;
import com.revature.repository.LikeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("likeService")
public class LikeService {
    private LikeDao likeDao;

    @Autowired
    public LikeService(LikeDao likeDao){this.likeDao = likeDao;}

    public Like toggleLike(Like like) {
        if (findLikeByUserIdAndPostId(like.getUser().getId(), like.getPost().getId()) == null) { //If the user hasn't liked this, like it. (save)
            return this.likeDao.save(like);
        } else {//Otherwise, unlike it (delete).
            this.likeDao.deleteById(findLikeByUserIdAndPostId(like.getUser().getId(), like.getPost().getId()).getId());
            return null;
        }
    }

    public List<Like> getAllLikes(){
        return this.likeDao.findAll();
    }

    public List<Like> findLikesByPostId(Integer postId){
        return this.likeDao.findLikesByPostId(postId);
    }

    public List<Like> findLikesByUserId(Integer userId){
        return this.likeDao.findLikesByPostId(userId);
    }
    public Like findLikeByUserIdAndPostId(Integer userId, Integer postId){
        return this.likeDao.findLikeByUserIdAndPostId(userId, postId);
    }

}
