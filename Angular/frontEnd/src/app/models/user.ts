
export class User{
    id: number = 0;
    username: string = '';
    password: string = '';
    firstName: string = '';
    lastName: string = '';
    url: string = ''
    email: string = ''


    constructor(id: number, username: string, password: string, firstName: string, lastName: string, url: string, email: string){
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.url = url;
        this.email = email;
    }
}
