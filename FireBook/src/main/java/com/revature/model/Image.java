package com.revature.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "images")
@Data
@NoArgsConstructor
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_id")
    private Integer id;
    @Column(name="image_url")
    private String url;
    @ManyToOne
    private Post post;

    public Image(String url, Post post) {
        this.url = url;
        this.post = post;
    }

}
